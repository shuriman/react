import { initializeApp } from "firebase/app";
import {getDatabase} from "firebase/database";
import {createUserWithEmailAndPassword, getAuth, signInWithEmailAndPassword, signOut as firebasesignOut} from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBDyGz_FrhXFoYtT523_QtixqcqgTW6trY",
    authDomain: "chats-b7ac2.firebaseapp.com",
    databaseURL: "https://chats-b7ac2-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "chats-b7ac2",
    storageBucket: "chats-b7ac2.appspot.com",
    messagingSenderId: "793940292881",
    appId: "1:793940292881:web:9c323c34b9d4dc8ee6c8b0"
};

initializeApp(firebaseConfig);

export const auth = getAuth();
export const db = getDatabase();

export const signUp = async (email, password) => {
    await createUserWithEmailAndPassword(auth, email, password);
}

export const login = async (email, password) =>{
    await signInWithEmailAndPassword(auth, email, password);
}

export const signOut = async () => {
    await firebasesignOut(auth);
}
