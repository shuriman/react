export const AUTHORS = {
    HUMAN: "human",
    BOT: "bot",
};

export const PUBLIC_URL = "https://api.spaceflightnewsapi.net/v3/articles";

export const REQUEST_STATUS = {
    IDLE: 0,
    LOADING: 1,
    SUCCESS: 2,
    FAILURE: 3,
}