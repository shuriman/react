import {onValue, ref, set} from "firebase/database";
import {db} from "../../services/firebase";
import {AUTHORS} from "../../utils/constants";

export const ADD_MESSAGE = 'MESSAGES::ADD_MESSAGE';
export const DEL_MESSAGE = 'MESSAGES::DEL_MESSAGE';
export const SET_MESSAGES = 'MESSAGES::SET_MESSAGES';

export const addMessage = (chatId, text, author) => ({
    type: ADD_MESSAGE,
    payload: {
        chatId,
        text,
        author,
    },
});

export const delMessage = (chatId, id) => ({
    type: DEL_MESSAGE,
    payload: {
        chatId,
        id,
    },
});

export const setMessages = (messages) => ({
    type: SET_MESSAGES,
    payload: messages,
});



let timeout;
export const addMessageWithReply = (chatId, text, author) => (dispath) => {
    dispath(addMessage(chatId, text, author));
    if (author === AUTHORS.HUMAN) {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            dispath(addMessage(chatId, 'Answer: test bot', AUTHORS.BOT));
        }, 3 * 1000);
    }
}

export const initMessages = () => (dispatch) =>{
    const messagesDbRef  = ref(db, `messages`);

    onValue(messagesDbRef, (snapshot) => {
        const data = snapshot.val();
        console.log('[-->', data);
        dispatch(setMessages(data || {}));
    });
}

export const addMessageFb = (text,author, chatId ) => (dispatch) => {
    let newId = `messages-${Date.now()}`;
    const messagesDbRef = ref(db, `messages/${chatId}/${newId}`)

    set(messagesDbRef, {
        author,
        text,
        id: newId,
    });

    newId = `messages-${Date.now()}`;
    const messagesDbRef1 = ref(db, `messages/${chatId}/${newId}`)
    if (author === AUTHORS.HUMAN) {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            set(messagesDbRef1, {
                author: AUTHORS.BOT,
                text: 'Answer: as11asa',
                id: newId,
            });
        }, 3 * 1000);
    }
}