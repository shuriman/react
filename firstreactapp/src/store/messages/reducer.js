import {ADD_MESSAGE, DEL_MESSAGE, SET_MESSAGES} from "./actions";
import {DEL_CHAT, ADD_CHAT} from "../chats/actions";

const initialState = {
    messages: {},
};

export const messagesReducer = (state = initialState, {type, payload}) => {
    switch (type){
        case ADD_MESSAGE:{
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [payload.chatId]: [...state.messages[payload.chatId] || [],
                        {
                            id: `message-${Date.now()}`,
                            text: payload.text,
                            author: payload.author,
                        },
                    ]
                },
            };
        }
        case ADD_CHAT:{
            return {
                ...state,
                messages: {
                    ...state.messages,
                },
            };
        }
        case DEL_CHAT:{
            const newMessages = {...state.messages};
            delete state.messages[payload];

            return {
                ...state,
                messages: newMessages,
            };
        }
        case SET_MESSAGES:{
            return {
                ...state,
                messages: payload,
            };
        }
        case DEL_MESSAGE:{
            const newMessages = state.messages[payload.chatId].filter(({id}) => id !== payload.id);

            return {
                ...state,
                messages: {
                    ...state.messages,
                    [payload.chatId]: newMessages,
                },
            }
        }

        default:
            return state;
    }
};