export const selectChats = state => state.chats.chats;
export const selectExistsChat = (id) => (state) => !!state.chats.chats.find((chat) => id === chat.id);