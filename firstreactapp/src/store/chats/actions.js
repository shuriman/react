import {onValue, ref, set} from "firebase/database";
import {db} from "../../services/firebase";

export const ADD_CHAT = "CHATS::ADD_CHAT";
export const DEL_CHAT = "CHATS::DEL_CHAT";
export const SET_CHATS = "CHATS::DEL_CHAT";

export const addChat = (name) => ({
    type: ADD_CHAT,
    payload: name
});

export const delChat = (name) => ({
    type: DEL_CHAT,
    payload: name
});

export const setChats = (chats) => ({
    type: SET_CHATS,
    payload: chats,
});

export const initChats = () => (dispatch) => {
    const chatsDbRef = ref(db, "chats");
    onValue(chatsDbRef, (snapshot) => {
        const data = snapshot.val();
        console.log('asas',data);
        //setChats(Object.values(data || {}));
        dispatch(setChats(Object.values(data || {})));
    });
};

export const addChatFb = (name) => () => {
    const newId = `chat-${Date.now()}`;
    const chatsDbRef = ref(db, `chats/${newId}`);

    set(chatsDbRef, {
        id: newId,
        name,
    });
};