import {combineReducers, createStore, applyMiddleware, compose} from "redux";
import { profileReducer } from "./profile/reducer";
import {chatsReducer} from "./chats/reducer";
import {messagesReducer} from "./messages/reducer";

import {articlesReducer} from "./articles/reducer";

import thunk from "redux-thunk";
import {persistStore, persistReducer} from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
    key: "gbTestID",
    storage,
};

const rootReducer = combineReducers({
    profile: profileReducer,
    chats: chatsReducer,
    messages: messagesReducer,
    articles: articlesReducer,

});

const persistedReducer = persistReducer(persistConfig, rootReducer);
const composeEnchancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE || compose;

export const store = createStore(
    persistedReducer,
    composeEnchancers(applyMiddleware(thunk))
    //window.__REDUX_DEVTOOLS_EXTENSION__ &&    window.__REDUX_DEVTOOLS_EXTENSION__()
);
export const persistor = persistStore(store);
