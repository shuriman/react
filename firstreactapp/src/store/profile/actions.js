import {onValue, ref, set} from "firebase/database";
import {db} from "../../services/firebase";

export const TOGGLE_SHOW_NAME = 'PROFILE::TOGGLE_SHOW_NAME';
export const CHANGE_NAME = 'PROFILE::CHANGE_NAME';
export const SET_USERNAME = 'PROFILE::SET_USERNAME';
export const GET_AUTH = 'PROFILE::GET_AUTH';
export const SET_AUTH = 'PROFILE::SET_AUTH';


export const changeName = (name) => ({
    type: CHANGE_NAME,
    payload: name,
});

export const getAuth = () => ({
    type: GET_AUTH,
});

export const setAuth = (authed) => ({
    type: SET_AUTH,
    payload: authed,
});

export const setUserName = (username) => (dispatch) => {
    set(ref(db, "user"), {
        username,
    });
    dispatch(changeName(username));

};




export const initUserName = () => (dispatch) =>{
    const userDbRef = ref(db,"user");

    onValue(userDbRef, (snapshot) => {
        const data = snapshot.val();
        dispatch(setUserName(data?.username || ''))
    });
}