import {CHANGE_NAME, SET_USERNAME, TOGGLE_SHOW_NAME, GET_AUTH, SET_AUTH} from "./actions";

const initialState = {
    showName: false,
    name: 'Test',
    authed: false,
}

export const profileReducer = (state = initialState, {type,payload}) => {
    switch(type){
        case TOGGLE_SHOW_NAME:{
            return {
                ...state,
                showName: !state.showName,
            }
        }
        case CHANGE_NAME:{
            return {
                ...state,
                name: payload,
            }
        }
        case SET_USERNAME:{
            return {
                ...state,
                name: payload,
            }
        }
        case GET_AUTH:{
            return {
                ...state,
                authed: state.authed,
            }
        }
        case SET_AUTH:{
            return {
                ...state,
                authed: payload,
            }
        }
        default:
            return state;
    }
}