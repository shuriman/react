export const selectProfiles = state => state.profile;
export const selectProfileName = state => state.profile.name;
export const selectAuthed = state => state.profile.authed;
