import {PUBLIC_URL} from "../../utils/constants";

export const GET_ARTICLES_LOADING = "ARTICLES::GET_LOADING";
export const GET_ARTICLES_SUCCESS = "ARTICLES::GET_SUCCESS";
export const GET_ARTICLES_FAILURE = "ARTICLES::GET_FAILURE";

const getArticlesLoading = (name) => ({
    type: GET_ARTICLES_LOADING,
});

const getArticlesSuccess = (articles) => ({
    type: GET_ARTICLES_SUCCESS,
    payload: articles,
});

const getArticlesFailure = (error) => ({
    type: GET_ARTICLES_FAILURE,
    payload: error,
});

export const getArticles = () => async (dispatch) => {
    dispatch(getArticlesLoading());

    try {
        const response = await fetch(PUBLIC_URL);
        if (!response.ok) {
            throw new Error(`1error ${response.status}`);
        }
        const result = response.json();

        dispatch(getArticlesSuccess(result));
    }
    catch (e){
        console.log(e);
        dispatch(getArticlesFailure(e.message));
    };

};