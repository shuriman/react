import {GET_ARTICLES_FAILURE, GET_ARTICLES_LOADING, GET_ARTICLES_SUCCESS} from "./actions";
import {REQUEST_STATUS} from "../../utils/constants";

const initialState = {
    list: [],
    request:{
        error: null,
        status: REQUEST_STATUS.IDLE,
    }
}

export const articlesReducer = (state = initialState, {type, payload}) => {
    switch (type){
        case GET_ARTICLES_LOADING:{
            return {
                ...state,
                request: {
                    error: null,
                    status: REQUEST_STATUS.LOADING,
                }
            };
        }
        case GET_ARTICLES_SUCCESS:{
            return {
                ...state,
                request: {
                    ...state.request,
                    status: REQUEST_STATUS.SUCCESS,
                },
                list: payload,
            };

        }
        case GET_ARTICLES_FAILURE:{
            return {
                ...state,
                request: {
                    error: payload,
                    status: REQUEST_STATUS.FAILURE,
                },
            };
        }
        default:
            return state;
    }
}