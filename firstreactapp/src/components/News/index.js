import React, {useEffect} from "react";
import {PUBLIC_URL} from "../../utils/constants";
import {CircularProgress} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getArticles} from "../../store/articles/actions";
import {selectArticles, selectArticlesError, selectArticlesLoading} from "../../store/articles/selectors";

export const News = () => {
    const dispatch = useDispatch();

    const error = useSelector(selectArticlesError);
    const loading = useSelector(selectArticlesLoading);
    const articles = useSelector(selectArticles);

    const reload = () => {
        dispatch(getArticles());
    };

    useEffect(() => {
        reload();
    }, [])

    return (

      <div>
          <h2>News</h2>
          {error ? (
              <>
                  <h3>Error: {error}</h3>
                    <button onClick={reload}>Refresh</button>
                </>
          ) : (
              <div></div>
              //     <article key={article.id}>
              //         <h4>{article.title}</h4>
              //     </article>
              // articles.map((article) => (
              //     <article key={article.id}>
              //         <h4>{article.title}</h4>
              //     </article>
              // ))
          )}
          {loading && <CircularProgress/>}
      </div>
    );

}