import React, {useEffect, useState} from "react";
import {List, Button} from "@material-ui/core";
import {ChatItem} from "../ChatItem";
import {ref, set, onValue} from "firebase/database";
import {db} from "../../services/firebase";


export const ChatList = ({ onAddChat, onDelChat}) => {
    const [chats, setChats] = useState([]);
    const [value, setValue] = useState('');

    const handlerChange = (e) => {
        setValue(e.target.value);
    }

    useEffect(() => {
       const chatsDbRef = ref(db, "chats");
       onValue(chatsDbRef, (snapshot) =>{
           const data = snapshot.val();
           console.log("|-->", data);
           setChats(Object.values(data || {}));
       })
    }, []);

    const handlerSubmit = (e) => {
        e.preventDefault();

        const newId = `chat-${Date.now()}`;
        const chatsDbRef = ref(db, `chats/${newId}`);

        set(chatsDbRef, {
            id: newId,
            name: value,
        });

        setValue("");
        //onAddChat(value);
    }

    return (
        <List>
                {chats.map((chat) => (
                    <ChatItem key={chat.id} chat={chat} onDeleteChat={onDelChat} id={chat.id}/>
                ))}
            <form onSubmit={handlerSubmit}>
                <input type="text" onChange={handlerChange}/>
                <Button disabled={!value} onClick={handlerSubmit}>
                    Add Chat
                </Button>
            </form>
        </List>
    );
}