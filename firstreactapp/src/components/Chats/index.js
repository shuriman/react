import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import {List, ListItem, TextField} from "@material-ui/core";
import {Message} from "../Message";
import {useParams, useHistory} from "react-router-dom";
import {ChatList} from "../ChatList";
import {useDispatch, useSelector} from "react-redux";
import {addChatFb, delChat, initChats} from "../../store/chats/actions";
import {addMessageFb,initMessages} from "../../store/messages/actions";
import {selectMessages} from "../../store/messages/selectors";
import {selectChats} from "../../store/chats/selectors";

import {
    ThemeProvider,
    useTheme,

    createTheme,
} from "@material-ui/core/styles";
import {AUTHORS} from "../../utils/constants";

const theme = createTheme({
    palette: {
        primary: {
            main: "#FF9800",
        },
        secondary: {
            main: "#0098FF",
        },
    },
});

export const Chats = () => {
    const {chatId} = useParams();
    const history = useHistory();

    const inputRef = useRef(null);
    const [value, setValue] = useState('')

    const dispatch = useDispatch();

    const messages = useSelector(selectMessages);
    const chats = useSelector(selectChats);


    useEffect(() => {

        dispatch(initChats());
        dispatch(initMessages());
    },[]);

    const sendMessage = useCallback(
         (text, author) => {
            dispatch(addMessageFb(text, author, chatId));
        },
        [chatId]);

    const handlerChange =  useCallback((event) =>{
        setValue(event.target.value);
    },[]);

    const handlerAddChat = ((e) =>{
        e.preventDefault();
        dispatch(addChatFb(value));
        setValue('');
    });

    const handlerDelChat = useCallback((id) =>{
        dispatch(delChat(id));
        if(chatId === id){
            history.push('/chats');
        }
    },[dispatch, chatId, history]);

    const handlerSubmit = (event) => {
        event.preventDefault();
        sendMessage(`Question: ${value}`, AUTHORS.HUMAN);
        setValue('');
        inputRef.current?.focus();
    };

    const chatExists = useMemo(() => !!chats.find(({id}) => id === chatId), [
       chatId,
        chats,
    ]);

    return (
        <ThemeProvider theme={theme}>
            <div className="App">

                <form onSubmit={handlerSubmit}>
                    <button>
                        Send message
                    </button>

                    <TextField
                        placeholder="message"
                        label="text"
                        value={value}
                        inputRef={inputRef}
                        onChange={handlerChange}
                    />

                </form>

                <div className="main">
                    <List subheader={<li />}>
                        {!!chatId && (
                            <>
                                {(Object.values(messages[chatId] || {}) || []).map((message) => (
                                    <li key={`section-${message.id}`}>
                                        <ul >
                                            <ListItem key={`item-${message.id}`}>
                                                <Message key={message.id} text = {message.text}/>
                                            </ListItem>
                                        </ul>
                                    </li>
                                ))}
                            </>
                        )}
                    </List>

                    <ChatList chats={chats} onAddChat={handlerAddChat} onDelChat={handlerDelChat}/>
                </div>

            </div>
        </ThemeProvider>
    );
}