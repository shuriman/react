import React from "react";
import {Button, ListItem} from "@material-ui/core";
import {Link} from "react-router-dom";


export const ChatItem = ({ chat, onDeleteChat, id }) => {

    const handlerDelete = () => {
        onDeleteChat(id);
    }

    return (
        <ListItem>
            <Link to={`/chats/${chat.id}`}>{chat.name}</Link>
            <Button onClick={handlerDelete}>
                Delete Chat
            </Button>
        </ListItem>
    );
}