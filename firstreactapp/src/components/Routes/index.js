import React, {useEffect, useState} from "react";
import {BrowserRouter, Switch, Route, Link} from "react-router-dom";
import {Home} from "../Home";
import {Chats} from "../Chats";
import {NotFound} from "../NotFound";
import {Profile} from "../Profile";
import {News} from "../News";
import {PrivateRoute} from "../PrivateRoute";
import {PublicRoute} from "../PublicRoute";
import {login, signUp, auth, signOut} from "../../services/firebase";

import { onAuthStateChanged } from 'firebase/auth';
import {useDispatch, useSelector} from "react-redux";
import {setAuth} from "../../store/profile/actions";

export const Routes = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            console.log(user);
            if(user){
                dispatch(setAuth(true));
            } else {
                dispatch(setAuth(false));
            }
        });

    }, []);

    const handlerLogin = async (email, password) => {

        try{
            await login(email, password);

        }catch (e){
            console.log(e);
        }

    };

    const handlerLogout = async () => {
        dispatch(setAuth(false));
        await signOut();
    };

    const handlerSignUp = async (email, password) =>{
        try{
            await signUp(email, password);
        }catch (e){
            console.log(e);
        }
    };

    return (
        <BrowserRouter>

            <ul>
                <li>
                    <Link to={`/chats`}>Chats</Link>
                </li>
                <li>
                    <Link to={`/profile`}>Profile</Link>
                </li>
                <li>
                    <Link to={`/news`}>News</Link>
                </li>
            </ul>

            <Switch>
                <PublicRoute path="/" exact>
                    <Home onLogin={handlerLogin}/>
                </PublicRoute>
                <PublicRoute path="/signup" exact>
                    <Home onSingUp={handlerSignUp}/>
                </PublicRoute>

                <PrivateRoute path="/chats/:chatId?" component={Chats}/>
                <PrivateRoute path="/profile" exact>
                    <Profile onLogout={handlerLogout}/>
                </PrivateRoute>
                <PublicRoute path="/news" component={News}/>
                <Route>
                   <NotFound/>
                </Route>



            </Switch>
        </BrowserRouter>
    );
}




