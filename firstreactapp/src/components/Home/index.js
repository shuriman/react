import React, {useState} from "react";


export const Home = ({onLogin, onSingUp}) => {

    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");


    const handleChangeLogin = (e) => {
        setLogin(e.target.value);
    }

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    }

    const  handleSubmit = (e) => {
        e.preventDefault();
        setLogin('');
        setPassword('');
        if(!!onLogin) {
            onLogin(login, password);
        }else{
            onSingUp(login,password);
        }
    }

    return (
        <>
            <h2>{!!onLogin ? 'Login' : 'SignUp'}</h2>
            <form onSubmit={handleSubmit}>
                <input type="text" value={login} onChange={handleChangeLogin}/>
                <input type="password" value={password} onChange={handleChangePassword}/>
                <button>Login</button>
            </form>
        </>

    );
}