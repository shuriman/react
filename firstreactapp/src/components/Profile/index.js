import React, {useCallback, useEffect, useState} from "react";
import {initUserName, setUserName} from "../../store/profile/actions";
import {useSelector, useDispatch} from "react-redux";
import {selectProfileName} from "../../store/profile/selectors";


export const Profile = ({onLogout}) => {
    const dispatch = useDispatch();
    const name = useSelector(selectProfileName);

    useEffect(() => {
        dispatch(initUserName());

    }, []);

    const handlerChange = useCallback((e) => {
        dispatch(setUserName(e.target.value));
    },[]);

    const handleClick = (e) => {
        onLogout();
    }

    return (
        <>
            <h3>Profile</h3>
            <button onClick={handleClick}>Logout</button>

            <form>
                <input
                    onChange={handlerChange}
                    type="text" />
                <div>{name}</div>
                <button type="submit">Submit</button>
            </form>

        </>
    );
}